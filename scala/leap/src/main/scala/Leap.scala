object Leap {
  val leapYear = leapYear1 _

  def leapYear1(year: Int): Boolean = {
    (year % 4 == 0, year % 100 == 0, year % 400 == 0) match {
      case (true, false, false) => true
      case (true, _, true) => true
      case _ => false
    }
  }

  def leapYear2(year: Int): Boolean = {
    year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)
  }
}
