import scala.collection.immutable.SortedMap

class School {
  type DB = Map[Int, Seq[String]]

  var aMap = Map.empty[Int, Seq[String]]

  def add(name: String, g: Int) = {
    val updatedMap = aMap.get(g).fold {
      aMap.updated(g, Seq(name))
    } { names =>
      aMap.updated(g, names :+ name)
    }
    aMap = updatedMap
  }

  def db: DB = aMap

  def grade(g: Int): Seq[String] = {
    aMap.get(g).fold(Seq.empty[String])(identity)
  }

  def sorted: DB = {
    val sortedMap = aMap.foldLeft(SortedMap.empty[Int, Seq[String]]) { (acc, kv) =>
      val (key, value) = kv
      acc.updated(key, value.sorted)
    }
    sortedMap.toMap
  }
}
