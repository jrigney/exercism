
object CollatzConjecture {
  def steps(n: Int): Option[Int] = {

    @scala.annotation.tailrec
    def loop(n: Int, acc: Int): Option[Int] = {

      // println(s"n $n acc $acc")

      n match {
        case n if n <=0 => None
        case 1 ⇒ Some(acc)
        case _ ⇒

          def isEven = n % 2 == 0
          isEven match {
            case true  ⇒ loop (n / 2, acc + 1)
            case false ⇒ loop (n * 3 + 1, acc + 1)
          }

      }
    }
    loop(n, 0)
  }
}
