
import scala.math.BigDecimal

object SpaceAge {

  def yearsInEarth(ageSeconds: BigDecimal): BigDecimal = {
    (ageSeconds / 31557600)
  }

  def convert(conversion: BigDecimal): BigDecimal => BigDecimal = (ageSeconds: BigDecimal) => {
    (yearsInEarth(ageSeconds) / conversion).setScale(2,BigDecimal.RoundingMode.HALF_DOWN)
  }

  val onEarth = convert(1)

  val onMercury = convert(0.2408467)

  val onVenus = convert(0.61519726)

  val onMars = convert(1.8808158)

  val onJupiter = convert(11.862615)

  val onSaturn = convert ( 29.447498 )

  val onUranus = convert ( 84.016846 )

  val onNeptune = convert (164.79132)

}
