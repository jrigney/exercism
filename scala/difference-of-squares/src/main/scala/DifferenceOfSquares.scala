object DifferenceOfSquares {

  def sumOfSquares(n: Int): Int = {
    1 to n map (x => x * x) sum
  }

  def squareOfSum(n: Int): Int = ???

  def differenceOfSquares(n: Int): Int = ???
}
