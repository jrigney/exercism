import scala.collection.immutable
object SumOfMultiples {
  val sum = sum1 _

  def sum1(factors: Set[Int], limit: Int): Int = {
    val xs = for {
      x <- 1 until limit
      y <- factors
      z <- if (x % y == 0) Some(x) else None
    } yield z
    xs.sum
  }

  def sum2(factors: Set[Int], limit: Int): Int = {
    (1 until limit).filter(x => factors.exists(x % _ == 0)).sum
  }

  def sum3(factors: Set[Int], limit: Int): Int = {
    factors.flatMap { f =>
      val multiples: Seq[Int] = f until limit by f
      multiples
    }.sum
  }
}

