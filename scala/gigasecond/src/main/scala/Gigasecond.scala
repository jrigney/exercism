import java.time._

object Gigasecond {
  val gigasecond = 1000000000L

  def add(startDate: LocalDate): LocalDateTime = {
    add(startDate.atStartOfDay())
  }

  def add(startDateTime: LocalDateTime): LocalDateTime = {
    startDateTime.plusSeconds(gigasecond)
  }
}


