
object Hamming {
  def distance(x: String, y: String): Option[Int] = {
    if (x.size != y.size) None
    else {
      val pairs = x.zip(y)
      val numMismatch = x.zip(y).count{ pair ⇒
        val (x, y) = pair
        x != y
      }
      Some(numMismatch)
    }
  }
}
