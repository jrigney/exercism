
object Etl {
  val transform = transform3 _

  def transform(oldData: Map[Int, Seq[String]]): Map[String, Int] = {
    oldData.foldLeft(Map.empty[String, Int]) { case (acc, (k, strings)) =>
      strings.foldLeft(acc) { (acc, string) =>
        acc.updated(string.toLowerCase, k)
      }
    }
  }

  def transform2(oldFormat: Map[Int, Seq[String]]): Map[String, Int] = {
    for {
      (score, letters) <- oldFormat
      letter <- letters
    } yield (letter.toLowerCase, score)
  }

  //transform3 broken down to into flatMaps
  def transform3(oldFormat: Map[Int, Seq[String]]): Map[String, Int] = {
    oldFormat.flatMap{case (k,strings) =>
        strings.map(s => (s.toLowerCase, k))
    }
  }
}
