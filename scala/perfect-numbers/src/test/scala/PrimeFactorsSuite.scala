import org.scalatest.{FunSuite, Matchers}

class PrimeFactorsSuite extends FunSuite with Matchers
{
  test("one") {
      val actual = PerfectNumbers.primeFactors(4)

      actual match {
        case Composite (is) => is.sorted == List(2,2)
        case _ => fail
      }
  }


  test("ten") {
    val actual = PerfectNumbers.primeFactors(10)

    actual match {
      case Composite (is) => is.sorted == List(2,5)
      case _ => fail
    }
  }

  test("twenty-eight") {
    val actual = PerfectNumbers.primeFactors(28)

    actual match {
      case Composite (is) => is.sorted == List(2,2,7)
      case _ => fail
    }
  }
}
