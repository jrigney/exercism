
import scala.annotation.tailrec

object PerfectNumbers {
  def classify(i: Int): Either[String, NumberType.Category] = {

    if (i <= 0) Left("Classification is only possible for natural numbers.")
    else if (i == 1) Right(NumberType.Deficient)
    else {
      val factor: (Int) => WholeNumber = factors _ compose primeFactors _

      factor(i) match {
        case Prime => Right(NumberType.Deficient)
        case Composite(factors) =>

          factors.sum match {
            case h if h == i => Right(NumberType.Perfect)
            case h if h < i => Right(NumberType.Deficient)
            case h if h > i => Right(NumberType.Abundant)

          }
      }

    }
  }

  def primeFactors(x: Int): WholeNumber = {

    @tailrec
    def loop(remaining: Int, guessedFactor: Int, acc: List[Int]): List[Int] = {

      //      println(s"remaining [${remaining}] guessedFactor [${guessedFactor}] acc [${acc}]")

      guessedFactor * guessedFactor > remaining match {
        case false if remaining % guessedFactor == 0 => loop(remaining / guessedFactor, guessedFactor, guessedFactor :: acc)
        case false => loop(remaining, guessedFactor + 1, acc)
        case true => remaining :: acc
      }

    }

    val factors = loop(x, 2, List.empty)
    if (factors.size == 1) Prime else Composite(factors)
  }

  private def factors(primeFactors: WholeNumber): WholeNumber = {

    primeFactors match {
      case a@Prime => a
      case Composite(primes) =>
        val combinations = (1 until primes.size).flatMap(i => primes.combinations(i).toList)

        val factors = (combinations.map(_.product).toList ++ primes)

        Composite((1 +: factors).distinct)
    }

  }
}

sealed trait WholeNumber

final case object Prime extends WholeNumber

final case class Composite(primeFactors: List[Int]) extends WholeNumber

object NumberType {

  sealed trait Category

  final case object Perfect extends Category

  final case object Abundant extends Category

  final case object Deficient extends Category

}
