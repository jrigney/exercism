object Bob {

  def response(statement: String): String = {
    val categorize = silence andThen shoutingQuestion andThen question andThen shout

    categorize(Left(statement)).getOrElse(Whatever) match {
      case Question => "Sure."
      case Shouting => "Whoa, chill out!"
      case ShoutingQuestion => "Calm down, I know what I'm doing!"
      case Silence => "Fine. Be that way!"
      case Whatever => "Whatever."
    }


  }

  type Category = Either[String, Statement]

  val question: Category => Category = {
    fakeLeftBias{
      case Left(s) if question_?(s) => Right(Question)
    }
  }

  val shout: Category => Category = {
    fakeLeftBias{
      case Left(x) if (shout_?(x)) => Right(Shouting)
    }
  }

  val shoutingQuestion: Category => Category = {
    fakeLeftBias{
      case Left(s) if shout_?(s) && question_?(s) => Right(ShoutingQuestion)
    }
  }

  val silence: Category => Category = {
    fakeLeftBias{
      case Left(s) if s.trim.isEmpty => Right(Silence)
    }
  }

  def question_?(s: String): Boolean = {
    s.trim.last == '?'
  }

  def shout_?(s: String): Boolean = {
    !s.exists(_.isLower) && s.exists(_.isUpper)
  }

  def fakeLeftBias(partialFunction: PartialFunction[Category, Category]): Category => Category = (c: Category) => {
    val otherwise: PartialFunction[Category, Category] = {
      case _ => c
    }
    val x = partialFunction orElse otherwise
    x.apply(c)
  }
}

sealed trait Statement

case object Question extends Statement

case object Shouting extends Statement

case object Silence extends Statement

case object Whatever extends Statement

case object ShoutingQuestion extends Statement
