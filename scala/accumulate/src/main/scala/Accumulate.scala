import scala.annotation.tailrec

class Accumulate {
  def accumulate[A, B](f: (A) => B, list : List[A]): List[B] = {

    @tailrec
    def loop(rest:List[A], f: A => B, acc: List[B]): List[B] = {
      rest match {
        case Nil => acc.reverse
        case h +: t =>
          loop(t, f, f(h) +: acc)
      }
    }

    loop(list, f, List.empty)
  }
}
