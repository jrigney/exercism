import scala.annotation.tailrec

object FlattenArray {
  def flatten(list: List[Any]): List[Int] = {
    // dfs so stack

    @tailrec
    def loop(fringe: List[Any], result: List[Int]): List[Int] = {
      fringe match {
        case Nil => result.reverse
        case h +: t =>
          h match {
            case a:Int => loop(t,a+:result)
            case a:List[Any] => loop(a ++ t, result)
            case _ => loop(t,result)
          }

      }
    }

    loop(list, List.empty)
  }
}
